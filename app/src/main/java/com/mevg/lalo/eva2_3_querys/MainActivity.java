package com.mevg.lalo.eva2_3_querys;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase db;
    TextView tvData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = openOrCreateDatabase("app", MODE_PRIVATE, null);
        tvData = (TextView) findViewById(R.id.data);
        db.execSQL("create table if not exists prros (" +
                "id integer primary key, " +
                "name text, " +
                "age integer, " +
                "phone text, " +
                "gender text )");
        db.execSQL("insert or replace into prros " +
                "values(1,'lalo', 22, '6141234567','male')");
        db.execSQL("insert or replace into prros " +
                "values(2,'ana', 22, '6141234567','female')");
        db.execSQL("insert or replace into prros " +
                "values(3,'alan', 20, '6147654321','male')");
        db.execSQL("insert or replace into prros " +
                "values(4,'majo', 20, '6147162534','female')");
        db.execSQL("insert or replace into prros " +
                "values(5,'pato', 19, '6141726354','female')");
        db.execSQL("insert or replace into prros " +
                "values(6,'lalo', 25, '6147653214','male')");
        //Cursor c = db.rawQuery("select * from prros where  id > ? and name = ? ", new String[] {"1", "lalo"});
        Cursor c = db.query("prros", new String[]{"id", "name", "age, gender"}," age > ?", new String[] {"20"},"gender",null,"name");
        c.moveToFirst();
        tvData.append(c.getColumnName(0) + " - "+ c.getColumnName(1) + " - "+ c.getColumnName(2) + " - " +
                c.getColumnName(3) + " - " +c.getColumnName(4) + "\n");
        while(!c.isAfterLast()){
            tvData.append(c.getInt(0) + " - " );
            tvData.append(c.getString(1) + " - " );
            tvData.append(c.getInt(2) + " - " );
            tvData.append(c.getString(3) + " - " );
            tvData.append(c.getString(4) + "\n"  );
            c.moveToNext();
        }
    }
}
